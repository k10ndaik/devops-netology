**/.terraform/*
все файлы каталога .terraform в любом месте или подкаталоге, где бы не находился сам каталог .terraform

*.tfstate
игнорировать все файлы с расширением  tfstate в папке где находится файл .gitignore 
*.tfstate.*
игнорировать все файлы с расширением tfstate и его под расширениями в папке где находится файл .gitignore 

crash.log
игнорировать все файл crash.log в папке где находится файл .gitignore 
  
*.tfvars
игнорировать все файлы с расширением tfvars в папке где находится файл .gitignore

override.tf
override.tf.json

игнорировать все файлы override.tf override.tf.json в папке где находится файл .gitignore

*_override.tf
*_override.tf.json

игнорировать все файлы оканчивающиеся на  override.tf override.tf.json в папке где находится файл .gitignore

.terraformrc
terraform.rc


игнорировать все файлы .terraformrc  terraform.rc в папке где находится файл .gitignore

